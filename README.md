# Mutual Aid App

A mutual aid app that would serve communities by allowing people to easily share resources and knowledge, respond to emergencies etc. with trusted members.

No code yet, see [Issues](https://gitlab.com/mechkit/mutual_aid_app/-/issues) for current research.
